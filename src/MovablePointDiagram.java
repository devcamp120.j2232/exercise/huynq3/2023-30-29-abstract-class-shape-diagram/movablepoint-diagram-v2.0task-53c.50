import model.MovableCycle;
import model.MovablePoint;

public class MovablePointDiagram {
    public static void main(String[] args) throws Exception {
        MovablePoint point = new MovablePoint(2, 3, 5, 1);
        MovablePoint center = new MovablePoint(1, 1, 2, 2);
        MovableCycle circle = new MovableCycle(10, center);
        System.out.println("Tọa độ điểm point ban đầu: " + point);
        System.out.println("Tọa độ tâm circle ban đầu: " + circle);
        point.moveUp();
        point.moveRight();
        circle.moveDown();
        circle.moveLeft();
        System.out.println("Tọa độ điểm point Mới: " + point);
        System.out.println("Tọa độ tâm circle Mới: " + circle);
    }
}
